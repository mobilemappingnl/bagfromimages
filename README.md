# BagFromImages

Cloned from the respository from [Raul Mur-Artal](https://github.com/raulmur/BagFromImages)

ROS package to generate a rosbag from a collection of images. Images are ordered alphabetically. The timestamp for each image is assigned according to the specified frequency. 
The bag will publish the images to topic `/cam0/image_raw`.
Tested in ROS Noetic in Ubuntu 20.04

## Prerequisite

Installation of [ROS Noetic](http://wiki.ros.org/noetic/Installation/Ubuntu) on your system (Ubuntu 20.04)

## Installation

In your ROS_PACKAGE_PATH (check your environment variable ROS_PACKAGE_PATH):

    git clone git@bitbucket.org:mobilemappingnl/bagfromimages.git BagFromImages
    cd BagFromImages
    mkdir build
    cd build
    cmake ..
    make

## Usage:
    (export ROS_PACKAGE_PATH=<BagFromImages dir>:$ROS_PACKAGE_PATH)
    roscore &
    rosrun BagFromImages BagFromImages PATH_TO_IMAGES IMAGE_EXTENSION FREQUENCY PATH_TO_OUPUT_BAG
  
 - `PATH_TO_IMAGES`: Path to the folder with the images
 - `IMAGE_EXTENSION`: .jpg, .png, etc. write the dot "."
 - `FREQUENCY`: Frames per second.
 - `PATH_TO_OUTPUT_BAG`: Path to save the bag (including the filename e.g. directory/filename.bag)

